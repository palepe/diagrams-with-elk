This is a tool for software engineers, who are interested in understanding system architecture by using generated system diagrams.

This project took an outset from https://github.com/OpenKieler/klassviz.

The following deviations have been made compared to the orignal project:  
0. Initial project setup problems are fixed. E.g. dependency to JAVA 9 and similar.  
1. There is no need to generate and save a diagram file on disk. Select the packages, you would like to overview and a new window opens up with the diagram.  
2. Can show class associations including 'uses' relationships. The original project supports only inheritance/extends and class property associations.  
3. Can show/hide methods.   
4. From the selected element in the given diagram, it can open the corresponding class file in the editor.  
5. Can show JAVA package dependency diagram.   

Project structure:  
1. bundles/ Eclipse plugin projects, containing business logic  
2. features/ & sites/ Eclipse features and sites, needed for building a installable product  
3. releng/ Maven infrustructure and target platform.  

Target platform  
The project is built on top of Eclipse Photon, Eclipse Layout Kernel and Kieler project.  

Maven  
Maven Tycho plugin is used to build Eclipse update site. A default "Build all" profile is part of the project.   
By running build all, you will get a installable update site under /com.palepe.classdiag.repository/target  

bitbucket  
Uses bitbucket.org as a code repository and CI tool.  

Features  
Show class or package dependency diagrams pop up menu  
![picture](images/pop-up.png)  
Show class diagram with 'uses' relationships  
![picture](images/uses-show-methods.png)  
The same diagram, but now with methods hidden (for better overview)  
![picture](images/uses-hide-methods.png)  
From the diagram element to the editor view with a "Show source" functionality  
![picture](images/show-source.png)  
Package dependency example  
![picture](images/package-dep.png)  





