/*
 * KlassViz - Kieler Class Diagram Visualization
 * 
 * A part of OpenKieler
 * https://github.com/OpenKieler
 * 
 * Copyright 2014 by
 * + Christian-Albrechts-University of Kiel
 *   + Department of Computer Science
 *     + Real-Time and Embedded Systems Group
 * 
 * This code is provided under the terms of the Eclipse Public License (EPL).
 * See the file epl-v10.html for the license text.
 */
package com.palepe.classdiag.synthesis

import de.cau.cs.kieler.klassviz.model.classdata.KMethod
import de.cau.cs.kieler.klassviz.model.classdata.KPackage
import de.cau.cs.kieler.klassviz.model.classdata.KType
import de.cau.cs.kieler.klassviz.model.classdata.KVisibility
import java.lang.reflect.GenericArrayType
import java.lang.reflect.Member
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType
import java.lang.reflect.TypeVariable
import java.lang.reflect.WildcardType
import org.eclipse.jdt.core.Flags
import org.eclipse.jdt.core.IMethod
import org.eclipse.jdt.core.Signature

/**
 * A collection of utility methods.
 * 
 * @containsExtensions
 */
final class ClassDataExtensions {
    
    /**
     * Build a qualified name for the given type.
     */
    def String getQualifiedName(KType type) {
        val result = new StringBuilder(type.name)
        var container = type.eContainer
        while (!(container instanceof KPackage) && container !== null) {
            if (container instanceof KType) {
                result.insert(0, '$')
                result.insert(0, (container as KType).name)
            }
            container = container.eContainer
        }
        if (container instanceof KPackage) {
            result.insert(0, '.')
            result.insert(0, (container as KPackage).name)
        }
        return result.toString
    }
    
    /**
     * Determine whether the signature of the given JDT method equals that of the KMethod.
     */
    def boolean equalSignature(IMethod jdtMethod, KMethod kMethod) {
        if (jdtMethod.elementName != kMethod.name
                || jdtMethod.numberOfParameters != kMethod.parameters.size) {
            return false
        }
        var int i = 0
        while (i < jdtMethod.numberOfParameters) {
            val jdtSignature = Signature.getSimpleName(Signature.toString(Signature.getTypeErasure(
                    jdtMethod.parameterTypes.get(i))))
            val kSignature = kMethod.parameters.get(i).signature
            if (jdtSignature != kSignature && jdtSignature != Signature.getSimpleName(kSignature)) {
                return false
            }
            i = i + 1
        }
        return true
    }
    
    /**
     * Determine whether the signature of the given Java method equals that of the KMethod.
     */
    def boolean equalSignature(Method method, KMethod kMethod) {
        if (method.name != kMethod.name
                || method.parameterTypes.length != kMethod.parameters.size) {
            return false
        }
        var int i = 0
        while (i < method.parameterTypes.length) {
            val clazz = method.parameterTypes.get(i)
            val kSignature = kMethod.parameters.get(i).signature
            if (clazz.simpleName != kSignature && clazz.name != kSignature) {
                return false
            }
            i = i + 1
        }
        return true
    }
    
    /**
     * Determine the visibility of the given type flags.
     */
    def KVisibility getVisibility(int flags) {
        if (Flags.isPublic(flags)) {
            KVisibility::PUBLIC
        } else if (Flags.isProtected(flags)) {
            KVisibility::PROTECTED
        } else if (Flags.isPrivate(flags)) {
            KVisibility::PRIVATE
        } else {
            KVisibility::PACKAGE
        }
    }
    
    /**
     * Determine whether the given member is declared static.
     */
    def isFinal(Member member) {
        Flags.isFinal(member.modifiers)
    }
    
    /**
     * Determine whether the given member is declared static.
     */
    def isAbstract(Member member) {
        Flags.isAbstract(member.modifiers)
    }
    
    /**
     * Compute a signature for the given parameterized type.
     */
    def dispatch CharSequence getSignature(ParameterizedType type) {
        '''«IF type.ownerType !== null
            »«type.ownerType.signature».«
        ENDIF»«
        type.rawType.signature
        »«IF type.actualTypeArguments.length > 0
            »<«type.actualTypeArguments.map[it.signature].join(", ")»>«
        ENDIF»'''
    }
    
    /**
     * Compute a signature for the given wildcard type.
     */
    def dispatch CharSequence getSignature(WildcardType type) {
        '''?«IF type.lowerBounds.length > 0
            » super «type.lowerBounds.map[it.signature].join(" | ")»«
        ENDIF»«IF type.upperBounds.length > 0
            » extends «type.upperBounds.map[it.signature].join(" | ")»«
        ENDIF»'''
    }
    
    /**
     * Compute a signature for the given generic array type.
     */
    def dispatch CharSequence getSignature(GenericArrayType type) {
        '''«type.genericComponentType.signature»[]'''
    }
    
    /**
     * Compute a signature for the given type variable.
     */
    def dispatch CharSequence getSignature(TypeVariable<?> typeVariable) {
        '''«typeVariable.name»«IF typeVariable.bounds.length > 0
            » extends «typeVariable.bounds.map[it.signature].join(" | ")»«
        ENDIF»'''
    }
    
    /**
     * Compute a signature for the given class or interface.
     */
    def dispatch CharSequence getSignature(Class<?> clazz) {
        clazz.name
    }
    
}
