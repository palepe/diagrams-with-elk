package com.palepe.classdiag.synthesis;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.SimpleType;

public class ASTTypeVisitor extends ASTVisitor {

	private final Set<String> types = new HashSet<>();
	@Override
	public boolean visit(SimpleType node) {
		getTypes().add(node.getName().getFullyQualifiedName());
		return true;
	}
	
	public Set<String> getTypes() {
		return types;
	}
}
