/*
 * KlassViz - Kieler Class Diagram Visualization
 * 
 * A part of OpenKieler
 * https://github.com/OpenKieler
 * 
 * Copyright 2014 by
 * + Christian-Albrechts-University of Kiel
 *   + Department of Computer Science
 *     + Real-Time and Embedded Systems Group
 * 
 * This code is provided under the terms of the Eclipse Public License (EPL).
 * See the file epl-v10.html for the license text.
 */
package com.palepe.classdiag.handlers;

import java.util.HashSet;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.inject.Guice;
import com.palepe.classdiag.synthesis.JdtModelTransformation;
import com.palepe.classdiag.ui.ClassDiagramViewPart;

import de.cau.cs.kieler.klassviz.model.classdata.ClassdataFactory;
import de.cau.cs.kieler.klassviz.model.classdata.KClassModel;
import de.cau.cs.kieler.klighd.ui.DiagramViewManager;
import de.cau.cs.kieler.klighd.ui.parts.DiagramViewPart;

/**
 * Handler to generate a KlassViz file from selected java packages, classes and/or classmembers.
 * The save location is queried from the user in a save as dialog and the selected items are serialized
 * into a .klaviz file with then can be visualized as a class diagram.
 */
public final class GenerateKlassVizFileHandler extends AbstractHandler {

    static final String PLUGIN_ID = "com.palepe.classdiag";
    final ClassdataFactory factory = ClassdataFactory.eINSTANCE;

    
    /**
     * {@inheritDoc}
     */
    public Object execute(ExecutionEvent event) throws ExecutionException {
        final IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
        final KClassModel classModel = generateClassModel(selection);
        final DiagramViewPart view = (DiagramViewPart)getView(ClassDiagramViewPart.VIEW_ID, true);
    	if (view.getViewContext() == null) {
    		view.initialize(classModel, null, null);
        } else {
            DiagramViewManager.updateView(view.getViewContext(), classModel);
        }
        return null;
    }
    
    private static ViewPart getView(String viewId, boolean forceVisible) {
        IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
        try {
            if (workbenchWindow == null) {
                return null;
            }
            IWorkbenchPage page = workbenchWindow.getActivePage();
            if (forceVisible) {
                return (ViewPart) page.showView(viewId, null, IWorkbenchPage.VIEW_VISIBLE);

            } else {
                IViewReference viewReference = page.findViewReference(viewId);
                if (viewReference != null) {
                    //if it's there, return it (but don't restore it if it's still not there).
                    //when made visible, it'll handle things properly later on.
                    return (ViewPart) viewReference.getView(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Generate a {@link KClassModel} from the given selection.
     * 
     * @param selection
     *            the selected java elements to generate the model from.
     * @return the generated model.
     */
    private KClassModel generateClassModel(IStructuredSelection selection) {
        
        // Transform the model
        JdtModelTransformation transformation = Guice.createInjector().getInstance(
                JdtModelTransformation.class);
        KClassModel classModel;
        try {
            classModel = transformation.transform(selection);
        } catch (JavaModelException exception) {
            IStatus status = new Status(IStatus.ERROR, PLUGIN_ID,
                    "Error while transforming Java model.", exception);
            StatusManager.getManager().handle(status, StatusManager.SHOW);
            return null;
        }

        // When the List is still empty, there was no IJavaElement in the
        // Selection and nothing can be serialized.
        if (!classModel.getPackages().isEmpty()) {
            // Save the selection.
            HashSet<IJavaProject> projects = new HashSet<IJavaProject>();
            Iterator<?> selectionIter = selection.iterator();
            while (selectionIter.hasNext()) {
                Object obj = selectionIter.next();
                if (obj instanceof IJavaElement) {
                    projects.add(((IJavaElement) obj).getJavaProject());
                }
            }
            for (IJavaProject project : projects) {
                classModel.getJavaProjects().add(project.getElementName());
            }
        }
        return classModel;
    }
}
