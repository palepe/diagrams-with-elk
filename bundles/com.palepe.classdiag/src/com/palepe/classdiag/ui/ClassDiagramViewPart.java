/*
 * KIELER - Kiel Integrated Environment for Layout Eclipse RichClient
 *
 * http://rtsys.informatik.uni-kiel.de/kieler
 * 
 * Copyright 2015 by
 * + Kiel University
 *   + Department of Computer Science
 *     + Real-Time and Embedded Systems Group
 * 
 * This code is provided under the terms of the Eclipse Public License (EPL).
 */
package com.palepe.classdiag.ui;

import de.cau.cs.kieler.klighd.ui.parts.DiagramViewPart;

/**
 * The view part that displays a graphical view of the currently selected variable.
 * 
 * @author cds
 */
public class ClassDiagramViewPart extends DiagramViewPart {
    
    /** The view part's ID as registered with Eclipse. */
    public static final String VIEW_ID = "com.palepe.classdiag.classView";
    


}
