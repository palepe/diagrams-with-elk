package com.palepe.classdiag.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import de.cau.cs.kieler.klassviz.model.classdata.KType;
import de.cau.cs.kieler.klighd.IAction;
import de.cau.cs.kieler.klighd.kgraph.KNode;

public class ShowSourceAction implements IAction {

    /**
     * The extension id of this actions. This id is to be mentioned in instances of
     * {@link de.cau.cs.kieler.klighd.krendering.KAction KAction}.
     */
    public static final String ID = "com.palepe.classdiag.actions.ShowSourceAction";
    
    /**
     * {@inheritDoc}
     */
    public ActionResult execute(final ActionContext context) { 
        final KNode kNode = context.getKNode();
        final Object property = context.getDomainElement(kNode);
        if (property instanceof KType) {
        	final KType typeModel = (KType) property;
        	try {
        		final String elementPathInWorkspace = typeModel.getElementPath();
        		if (elementPathInWorkspace != null) {
        			final IPath path = new Path(elementPathInWorkspace);
            		final IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
            		final IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file.getName());
            		final IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
            		page.openEditor(new FileEditorInput(file), desc.getId());        			
        		}
			} catch (PartInitException e) {
				throw new RuntimeException(e);
			}
        }
        return null;
    }

}
